import 'package:test_flutter/converters.dart';

class Article {
  final String uuid;
  final String libelle;
  final String quantite;
  final bool pris;
  final int horodatageModification;
  final bool supprime;

  Article(this.uuid, this.libelle, this.quantite)
      : pris = false,
        horodatageModification = DateTime.now().millisecondsSinceEpoch,
        supprime = false;

  Article.toggleable(this.uuid, this.libelle, this.quantite, this.pris)
      : horodatageModification = DateTime.now().millisecondsSinceEpoch,
        supprime = false;

  Article.deleted(this.uuid, this.libelle, this.quantite, this.pris)
      : horodatageModification = DateTime.now().millisecondsSinceEpoch,
        supprime = true;

  const Article.full(this.uuid, this.libelle, this.quantite, this.pris,
      this.horodatageModification, this.supprime);

  Map<String, dynamic> toMap() {
    return {
      'uuid': uuid,
      'libelle': libelle,
      'quantite': quantite,
      'horodatageModification': horodatageModification,
      'supprime': boolToInt(supprime),
      'pris': boolToInt(pris),
    };
  }

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'libelle': libelle,
      'quantite': quantite,
      'horodatageModification': horodatageModification,
      'supprime': supprime,
      'pris': pris,
    };
  }

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article.full(json['uuid'], json['libelle'], json['quantite'],
        json['pris'], json['horodatageModification'], json['supprime']);
  }

  Article copyWith(
      {String? uuid,
      String? libelle,
      String? quantite,
      bool? supprime,
      bool? pris}) {
    return Article.full(
        uuid ?? this.uuid,
        libelle ?? this.libelle,
        quantite ?? this.quantite,
        pris ?? this.pris,
        DateTime.now().millisecondsSinceEpoch,
        supprime ?? this.supprime);
  }

  @override
  String toString() {
    return """
      uuid=$uuid libelle=$libelle quantite=$quantite horodatageModification=$horodatageModification supprime=$supprime pris=$pris
    """;
  }
}
