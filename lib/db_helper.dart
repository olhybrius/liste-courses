import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_flutter/article.dart';

import 'converters.dart';

const TABLE_NAME = 'articles';

Future<Database> _getDbConnection() async {
  return openDatabase(
    join(await getDatabasesPath(), 'database.db'),
    onCreate: (db, version) {
      return _createTable(db);
    },
    version: 1,
  );
}

Future<void> _createTable(Database database) async {
  await database.execute(
      'CREATE TABLE $TABLE_NAME(uuid TEXT PRIMARY KEY, libelle TEXT, quantite TEXT, pris INTEGER, horodatageModification INTEGER, supprime INTEGER)');
}

Future<List<Article>> getActiveArticles() async {
  final db = await _getDbConnection();
  final List<Map<String, dynamic>> maps =
      await db.query(TABLE_NAME, where: 'supprime = 0');
  return List.generate(maps.length, (i) {
    return Article.full(
      maps[i]['uuid'],
      maps[i]['libelle'],
      maps[i]['quantite'],
      intToBool(maps[i]['pris']),
      maps[i]['horodatageModification'],
      intToBool(maps[i]['supprime']),
    );
  });
}

Future<List<Article>> getAllArticles() async {
  final db = await _getDbConnection();
  final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME);
  return List.generate(maps.length, (i) {
    return Article.full(
      maps[i]['uuid'],
      maps[i]['libelle'],
      maps[i]['quantite'],
      intToBool(maps[i]['pris']),
      maps[i]['horodatageModification'],
      intToBool(maps[i]['supprime']),
    );
  });
}

Future<void> insertArticle(Article article) async {
  final db = await _getDbConnection();
  await db.insert(TABLE_NAME, article.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<void> updateArticle(Article article) async {
  final db = await _getDbConnection();
  await db.update(TABLE_NAME, article.toMap(),
      where: 'uuid = ?', whereArgs: [article.uuid]);
}

Future<void> deleteArticle(String uuid) async {
  final db = await _getDbConnection();
  await db.delete(TABLE_NAME, where: 'uuid = ?', whereArgs: [uuid]);
}
