import 'package:flutter/material.dart';

class SuppressionDialog extends StatelessWidget {
  const SuppressionDialog({Key? key, required this.suppressionCallback}): super(key: key);

  final Function suppressionCallback;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Confirmation de suppression"),
      content: Text("Etes-vous sûr ?"),
      actions: [
        TextButton(
            onPressed: () => {Navigator.pop(context, 'Cancel')},
            child: Text("Non")),
        TextButton(
            onPressed: () =>
            {Navigator.pop(context, 'Confirm'), suppressionCallback()},
            child: Text("Oui")),
      ],
    );
  }
}