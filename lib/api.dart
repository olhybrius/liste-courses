import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:http/http.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:test_flutter/http_client.dart';

import 'article.dart';
import 'date_reference.dart';

const _API_ROOT = String.fromEnvironment('API_URL');
final HttpClient _httpClient = new HttpClient();

abstract class ApiResponse<T> {
  bool isError = false;
  int statusCode = 200;
}

class ValidResponse<T> extends ApiResponse<T> {
  T? data;

  ValidResponse(int statusCode, T? data) {
    this.statusCode = statusCode;
    this.data = data;
    this.isError = false;
  }
}

class ErrorResponse<T> extends ApiResponse<T> {
  String errorMessage = '';

  ErrorResponse(int statusCode, String errorMessage) {
    this.statusCode = statusCode;
    this.errorMessage = errorMessage;
    this.isError = true;
  }
}

Future<ApiResponse<T>> _tryNetworkRequest<T>(Function request) async {
  try {
    return await request();
  } on SocketException catch (socketException, stackTrace) {
    await Sentry.captureException(
      socketException,
      stackTrace: stackTrace,
    );
    return ErrorResponse(503, 'Connexion impossible.');
  }
}

Article _parseArticle(Response response) =>
    Article.fromJson(_decodeUtf8Json(response.bodyBytes));

Future<ApiResponse<List<Article>>> fetchArticles() async {
  return _tryNetworkRequest<List<Article>>(() async {
    final response = await _httpClient.get(Uri.parse('$_API_ROOT/articles/'));
    List<Article> parseArticles(Response response) => List<Article>.from(
        _decodeUtf8Json(response.bodyBytes).map((article) => Article.fromJson(article)));
    return _makeApiResponse(response, parseArticles);
  });
}

Future<ApiResponse<void>> createArticle(Article article) async {
  return _tryNetworkRequest(() async {
    final response = await _httpClient.post(
      Uri.parse('$_API_ROOT/articles/'),
      body: jsonEncode(article.toJson()),
    );
    return _makeApiResponse(response, _parseArticle);
  });
}

Future<ApiResponse<void>> updateArticle(Article article) async {
  return _tryNetworkRequest(() async {
    final response = await _httpClient.put(
      Uri.parse('$_API_ROOT/articles/${article.uuid}/'),
      body: jsonEncode(article.toJson()),
    );
    return _makeApiResponse(response, _parseArticle);
  });
}

Future<ApiResponse<void>> deleteArticle(Article article) async {
  return _tryNetworkRequest(() async {
    final response = await _httpClient.patch(
      Uri.parse('$_API_ROOT/articles/${article.uuid}/supprimer/'),
      body: jsonEncode(article.toJson()),
    );
    return _makeApiResponse(response, _parseArticle);
  });
}

Future<ApiResponse<DateReference>> getDateReference() async {
  return _tryNetworkRequest(() async {
    final response =
        await _httpClient.get(Uri.parse('$_API_ROOT/date-reference'));
    DateReference parseDateReference(Response r) =>
        List<DateReference>.from(_decodeUtf8Json(response.bodyBytes)
            .map<DateReference>((json) => DateReference.fromJson(json))).first;
    return _makeApiResponse(response, parseDateReference);
  });
}

ApiResponse<T> _makeApiResponse<T>(
    Response response, T Function(Response response) parsingFunction) {
  if (response.statusCode >= 300) {
    return ErrorResponse(response.statusCode, _decodeUtf8Json(response.bodyBytes)['detail']);
  }
  T apiResponseBody = parsingFunction(response);
  return ValidResponse(response.statusCode, apiResponseBody);
}

dynamic _decodeUtf8Json(Uint8List bytes) {
  return jsonDecode(utf8.decode(bytes));
}