import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:badges/badges.dart';
import 'package:test_flutter/suppression_alert.dart';
import 'package:test_flutter/sync.dart';

import 'article.dart';
import 'db_helper.dart';
import 'details.dart';

class ListeCourses extends StatefulWidget {
  ListeCourses({Key? key, required this.toggleThemeCallback}) : super(key: key);

  final Function toggleThemeCallback;

  @override
  _ListeCoursesState createState() => _ListeCoursesState();
}

class _ListeCoursesState extends State<ListeCourses> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  List<Article> entries = [];
  List<Article> selectedArticles = [];
  bool syncNeeded = false;
  bool previousSyncNeeded = false;

  Future<void> populateList() async {
    SyncResult syncResult = await synchronize();
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(syncResult.message),
      backgroundColor:
          syncResult.status == SyncStatus.success ? null : Colors.red,
    ));
    List<Article> articles = await getActiveArticles();
    setState(() {
      syncNeeded = false;
      entries = articles;
    });
  }

  void addItem(Article article) async {
    await insertArticle(article);
    setState(() {
      entries.add(article);
      syncNeeded = true;
    });
  }

  void updateItem(Article article) async {
    await updateArticle(article);
    var articleIndex =
        entries.indexWhere((element) => element.uuid == article.uuid);
    setState(() {
      entries[articleIndex] = article;
      syncNeeded = true;
    });
  }

  Future<void> deleteItem(Article article) async {
    await updateArticle(article);
    setState(() {
      entries.removeWhere((element) => element.uuid == article.uuid);
      syncNeeded = true;
    });
  }

  void reinsertItem(Article article, int index) async {
    await insertArticle(article);
    setState(() {
      entries.insert(index, article);
      syncNeeded = previousSyncNeeded;
    });
  }

  Widget getText(Article article) {
    return Text('${article.libelle} (${article.quantite})',
        style: TextStyle(
            decoration: TextDecoration.lineThrough, decorationThickness: 2.85));
  }

  void toggleSelection(Article article) {
    setState(() {
      if (selectedArticles.contains(article))
        selectedArticles.remove(article);
      else
        selectedArticles.add(article);
    });
  }

  void _deleteSelection() async {
    final futures = selectedArticles.map((e) async {
      final articleToDelete = e.copyWith(supprime: true);
      await deleteItem(articleToDelete);
    });
    await Future.wait(futures);
    selectedArticles.clear();
  }

  @override
  initState() {
    super.initState();
    // wait for widgets to be done with rendering
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      _refreshIndicatorKey.currentState?.show();
    });
    final port = ReceivePort();
    IsolateNameServer.registerPortWithName(port.sendPort, backgroundSyncPort);
    port.listen((dynamic data) async {
      _refreshIndicatorKey.currentState?.show();
    });
  }

  Widget _getSyncButton() {
    return IconButton(
        icon: const Icon(Icons.sync),
        tooltip: 'Synchroniser',
        onPressed: () {
          _refreshIndicatorKey.currentState!.show();
        });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: selectedArticles.isEmpty
          ? AppBar(
              leading: IconButton(
                icon: const Icon(Icons.brightness_4),
                onPressed: () {
                  widget.toggleThemeCallback();
                },
              ),
              title: Center(child: Text('Liste de courses')),
              automaticallyImplyLeading: false,
              actions: <Widget>[
                  syncNeeded
                      ? Badge(
                          position: BadgePosition.topEnd(top: 15, end: 11),
                          child: _getSyncButton())
                      : _getSyncButton()
                ])
          : AppBar(
              leading: IconButton(
                onPressed: () {
                  setState(() {
                    selectedArticles.clear();
                  });
                },
                icon: const Icon(Icons.arrow_back),
              ),
              actions: [
                IconButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (_) => SuppressionDialog(
                                suppressionCallback: _deleteSelection,
                              ));
                    },
                    icon: const Icon(Icons.delete))
              ],
            ),
      body: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: populateList,
          child: ListView.separated(
            itemCount: entries.length,
            itemBuilder: (BuildContext context, int index) {
              final item = entries[index];
              return Dismissible(
                  key: UniqueKey(),
                  dismissThresholds: {DismissDirection.startToEnd: 0.6},
                  background: Container(
                      color: Colors.red,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      alignment: AlignmentDirectional.centerStart,
                      child: Icon(Icons.delete, color: Colors.white)),
                  direction: DismissDirection.startToEnd,
                  onDismissed: (DismissDirection direction) async {
                    final articleToDelete = item.copyWith(supprime: true);
                    previousSyncNeeded = syncNeeded;
                    await deleteItem(articleToDelete);
                    ScaffoldMessenger.of(context).removeCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                          content: const Text('Article supprimé'),
                          action: SnackBarAction(
                            label: 'Annuler',
                            onPressed: () {
                              reinsertItem(item, index);
                            },
                          )),
                    );
                  },
                  child: ListTile(
                    selectedTileColor: Colors.lightBlue,
                    selectedColor: Colors.white,
                    selected: selectedArticles.contains(item),
                    onLongPress: () => toggleSelection(item),
                    leading: Checkbox(
                      onChanged: (bool? value) {
                        updateItem(item);
                      },
                      value: item.pris,
                    ),
                    title: Text('${item.libelle} (${item.quantite})'),
                    onTap: selectedArticles.isEmpty
                        ? () {
                            final article = item.copyWith(pris: !item.pris);
                            updateItem(article);
                          }
                        : () => toggleSelection(item),
                    trailing: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ArticleDetails(
                                          notifyParent: updateItem,
                                          deleteFunction: deleteItem,
                                          article: item)))
                            }),
                  ));
            },
            separatorBuilder: (BuildContext context, int index) {
              // Remove top and bottom padding
              return Divider(height: 1);
            },
          )),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ArticleDetails(notifyParent: addItem)),
            );
          },
          child: const Icon(Icons.add)),
    );
  }
}
