import 'package:collection/collection.dart';
import 'package:test_flutter/api.dart' as api;
import 'package:test_flutter/date_reference.dart';
import 'package:test_flutter/db_helper.dart' as localDb;
import 'article.dart';

const backgroundSyncTask = "backgroundSyncTask";
const backgroundSyncPort = "backgroundSyncPort";

class SyncResult {
  final SyncStatus status;
  final String message;

  SyncResult(this.status, this.message);

  SyncResult.withoutMessage(this.status) : message = '';

  @override
  String toString() {
    return 'status=$status, message=$message';
  }
}

enum SyncStatus { success, error }

Future<SyncResult> synchronize() async {
  var localArticles = await localDb.getAllArticles();
  final fetchArticleApiResponse = await api.fetchArticles();

  if (fetchArticleApiResponse.isError)
    return _handleError(fetchArticleApiResponse,
        'Une erreur est survenue lors de la récupération de la liste des articles');
  var remoteArticles = (fetchArticleApiResponse as api.ValidResponse).data;

  final getDateReferenceApiResponse = await api.getDateReference();

  if (getDateReferenceApiResponse.isError)
    return _handleError(getDateReferenceApiResponse,
        'Une erreur est survenue lors de la récupération de la date de référence');
  final dateReference = (getDateReferenceApiResponse as api.ValidResponse).data;

  localArticles =
      await _doLocalSync(localArticles, remoteArticles, dateReference);
  final syncResults = await _doRemoteSync(localArticles, remoteArticles);

  return syncResults.firstWhere(
      (syncResult) => syncResult.status == SyncStatus.error,
      orElse: () => SyncResult(
          SyncStatus.success, 'Synchronisation effectuée avec succès !'));
}

Future<List<Article>> _doLocalSync(List<Article> localArticles,
    List<Article> remoteArticles, DateReference dateReference) async {
  final updatedLocalArticles = await _removeDeletedArticles(
      localArticles, remoteArticles, dateReference);
  await _createMissingLocalArticles(localArticles, remoteArticles);
  await _updateLocalArticles(localArticles, remoteArticles);
  return updatedLocalArticles;
}

Future<List<SyncResult>> _doRemoteSync(
    List<Article> localArticles, List<Article> remoteArticles) async {
  SyncResult createSyncResult =
      await _createMissingRemoteArticles(localArticles, remoteArticles);
  SyncResult deleteSyncResult =
      await _updateRemoteArticlesToSoftDelete(localArticles, remoteArticles);
  SyncResult updateSyncResult =
      await _updateRemoteArticles(localArticles, remoteArticles);

  return [createSyncResult, deleteSyncResult, updateSyncResult];
}

Future<void> _createMissingLocalArticles(
    List<Article> localArticles, List<Article> remoteArticles) async {
  var articlesToCreate = remoteArticles
      .where((element) => !localArticles.any((el) => element.uuid == el.uuid));
  await Future.wait(articlesToCreate
      .map((element) async => await localDb.insertArticle(element)));
}

Future<SyncResult> _createMissingRemoteArticles(
    List<Article> localArticles, List<Article> remoteArticles) async {
  var articlesToCreate = localArticles
      .where((element) => !remoteArticles.any((el) => element.uuid == el.uuid));
  List<api.ApiResponse> apiResponses = await Future.wait(articlesToCreate
      .map((element) async => await api.createArticle(element)));
  return _getSyncResult(
      apiResponses, 'Erreur lors de la création des articles distants');
}

Future<void> _updateLocalArticles(
    List<Article> localArticles, List<Article> remoteArticles) async {
  var articlesToUpdate =
      _identifyArticlesToUpdate(localArticles, remoteArticles);
  await Future.wait(articlesToUpdate
      .map((article) async => await localDb.updateArticle(article)));
}

Future<SyncResult> _updateRemoteArticles(
    List<Article> localArticles, List<Article> remoteArticles) async {
  var articlesToUpdate =
      _identifyArticlesToUpdate(remoteArticles, localArticles);
  List<api.ApiResponse> apiResponses = await Future.wait(articlesToUpdate
      .map((article) async => await api.updateArticle(article)));
  return _getSyncResult(
      apiResponses, 'Erreur lors de la mise à jour des articles distants');
}

List<Article> _identifyArticlesToUpdate(
    List<Article> source, List<Article> target) {
  return target
      .where((targetArticle) => source.any((sourceArticle) =>
          targetArticle.uuid == sourceArticle.uuid &&
          sourceArticle.horodatageModification <
              targetArticle.horodatageModification))
      .toList();
}

Future<SyncResult> _updateRemoteArticlesToSoftDelete(
    List<Article> localArticles, List<Article> remoteArticles) async {
  var articlesToUpdate = localArticles.where((element) => element.supprime);
  List<api.ApiResponse> apiResponses = await Future.wait(articlesToUpdate
      .map((article) async => await api.deleteArticle(article)));
  return _getSyncResult(
      apiResponses, 'Erreur lors de la suppression des articles distants');
}

Future<List<Article>> _removeDeletedArticles(List<Article> localArticles,
    List<Article> remoteArticles, DateReference dateReference) async {
  var articlesToRemove = localArticles
      .where((element) =>
          element.supprime &&
          !remoteArticles.any((e) => e.uuid == element.uuid) &&
          element.horodatageModification < dateReference.valeur)
      .toList();
  await Future.wait(articlesToRemove
      .map((article) async => await localDb.deleteArticle(article.uuid)));
  List<Article> newList = List.from(localArticles);
  newList.removeWhere(
      (element) => articlesToRemove.any((e) => e.uuid == element.uuid));
  return newList;
}

SyncResult _getSyncResult(
    List<api.ApiResponse> apiResponses, String errorMessage) {
  final firstError =
      apiResponses.firstWhereOrNull((apiResponse) => apiResponse.isError);
  if (firstError != null) return SyncResult(SyncStatus.error, errorMessage);
  return SyncResult.withoutMessage(SyncStatus.success);
}

SyncResult _handleError(api.ApiResponse apiResponse, String errorMessage) {
  return SyncResult(SyncStatus.error,
      '$errorMessage : ${(apiResponse as api.ErrorResponse).errorMessage} (${apiResponse.statusCode})');
}
