class DateReference {
  final int valeur;

  DateReference(this.valeur);

  factory DateReference.fromJson(Map<String, dynamic> json) {
    return DateReference(json['valeur']);
  }

  @override
  String toString() {
    return 'valeur=$valeur';
  }
}
