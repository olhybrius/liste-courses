import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test_flutter/sync.dart';
import 'package:test_flutter/themes.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'package:workmanager/workmanager.dart';

import 'liste_courses.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager().initialize(callbackDispatcher,
      isInDebugMode:
          kDebugMode // If enabled it will post a notification whenever the task is running. Handy for debugging tasks
      );
  Workmanager().registerPeriodicTask("1", backgroundSyncTask,
      constraints: Constraints(
          networkType: NetworkType.connected,
          requiresBatteryNotLow: true,
          // Enabling the battery saving mode on the android device prevents the job from running
          requiresStorageNotLow: true));
  await SentryFlutter.init(
        (options) {
      options.dsn = 'https://ab68112049214c06a4302ee32dd91043@o1165675.ingest.sentry.io/6255833';
      // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
      // We recommend adjusting this value in production.
      options.tracesSampleRate = 1.0;
    },
    appRunner: () => runApp(MyApp()),
  );
}

void callbackDispatcher() {
  Workmanager().executeTask((task, inputData) async {
    print(
        "Native called background task: $task"); //simpleTask will be emitted here.
    switch (task) {
      case backgroundSyncTask:
        final sendPort = IsolateNameServer.lookupPortByName(backgroundSyncPort);
        sendPort?.send({});
        break;
    }
    return Future.value(true);
  });
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _appState();
}

class _appState extends State<MyApp> {
  bool _isLightTheme = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Liste de courses',
      theme: _isLightTheme ? lightTheme : darkTheme,
      home: ListeCourses(toggleThemeCallback: _toggleTheme,),
    );
  }

  void _toggleTheme() {
    setState(() {
      _isLightTheme = !_isLightTheme;
    });
  }
}
