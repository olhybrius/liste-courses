bool intToBool(int i) {
  return i == 1;
}

int boolToInt(bool b) {
  return b == true ? 1 : 0;
}