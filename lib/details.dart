import 'package:flutter/material.dart';
import 'package:test_flutter/suppression_alert.dart';
import 'package:uuid/uuid.dart';

import 'article.dart';

class ArticleDetails extends StatefulWidget {
  const ArticleDetails(
      {Key? key, required this.notifyParent, this.deleteFunction, this.article})
      : super(key: key);

  final Article? article;
  final Function notifyParent;
  final Function? deleteFunction;

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<ArticleDetails> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  final libelleController = TextEditingController();
  final quantiteController = TextEditingController();

  List<Widget> createAppBarActions() {
    List<Widget> actions = [];
    if (widget.article != null) {
      actions.add(IconButton(onPressed: _showDialog, icon: Icon(Icons.delete)));
    }
    actions.add(IconButton(onPressed: saveArticle, icon: Icon(Icons.save)));
    return actions;
  }

  _showDialog() {
    return showDialog(
        context: context,
        builder: (_) => SuppressionDialog(suppressionCallback: deleteArticle));
  }

  void deleteArticle() {
    var uuid = widget.article!.uuid;
    var pris = widget.article!.pris;
    var article = new Article.deleted(
        uuid, libelleController.text, quantiteController.text, pris);
    widget.deleteFunction!(article);
    Navigator.pop(context);
  }

  String _getQuantite() {
    if (quantiteController.text.isEmpty) return "1";
    return quantiteController.text;
  }

  void saveArticle() {
    if (_formKey.currentState!.validate()) {
      var article;
      var uuid;
      final quantite = _getQuantite();
      if (widget.article == null) {
        uuid = Uuid().v4();
        article = new Article(uuid, libelleController.text, quantite);
      } else {
        uuid = widget.article!.uuid;
        var pris = widget.article!.pris;
        article = new Article.toggleable(
            uuid, libelleController.text, quantite, pris);
      }
      widget.notifyParent(article);
      Navigator.pop(context);
    }
  }

  String? _validateRequiredField(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champ est requis';
    }
    return null;
  }

  @override
  void dispose() {
    libelleController.dispose();
    quantiteController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.article != null) {
      libelleController.text = widget.article!.libelle;
      quantiteController.text = widget.article!.quantite;
    }

    // Build a Form widget using the _formKey created above.
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.article?.libelle ?? 'Nouvel article'),
          actions: createAppBarActions(),
        ),
        body: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(children: [
                    TextFormField(
                      decoration: const InputDecoration(labelText: 'Libellé'),
                      controller: libelleController,
                      validator: _validateRequiredField,
                    ),
                    TextFormField(
                      decoration: const InputDecoration(labelText: 'Quantité'),
                      controller: quantiteController,
                    ),
                  ]))
              // Add TextFormFields and ElevatedButton here.
            ],
          ),
        ));
  }
}
