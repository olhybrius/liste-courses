import 'package:flutter/material.dart';

ThemeData lightTheme = ThemeData(primarySwatch: Colors.blue);
ThemeData darkTheme = ThemeData(
    colorScheme: ColorScheme.fromSwatch(brightness: Brightness.dark, primarySwatch: Colors.blue, accentColor: Colors.blue),
    appBarTheme: AppBarTheme(backgroundColor: Colors.blue));
