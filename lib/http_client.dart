import 'package:http/http.dart';

class HttpClient extends BaseClient {
  static Map<String, String> _getHeaders() {
    return {
      'Authorization': 'Token ${const String.fromEnvironment('AUTH_TOKEN')}',
      'Content-Type': 'application/json; charset=UTF-8'
    };
  }

  @override
  Future<StreamedResponse> send(BaseRequest request) async {
    request.headers.addAll(_getHeaders());
    return request.send();
  }
}
